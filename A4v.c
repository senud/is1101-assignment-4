 #include <stdio.h>
  int main () {
        int i, j, n;
        printf("Enter the value for n:");
        scanf("%d", &n);

        for (i = 1; i <= n; i++) {
                printf("Multiplicaton table for %d\n", i);
                for (j = 1; j <= 10; j++) {
                       printf("%2d X %2d = %3d\n", i, j, i * j);
                }
                printf("\n");
        }
        return 0;
  }


